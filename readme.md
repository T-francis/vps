# John

Your vps user

## Install & usage

### Get source files

git clone https://T-francis@bitbucket.org/T-francis/vps.git /tmp \
 && cd /tmp

### Set user and permission

```bash
export username=john \
&& apt-get -y install sudo git wget curl jq \
&& adduser ${username} \
&& usermod -a -G root,adm,sudo ${username} \
&& exit
```
