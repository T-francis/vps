# Traefik 2 - Reverse Proxy
version: "3"

services:

  traefik:
    container_name: traefik
    restart: always
    image: traefik:2.7
    environment:
      - DOMAIN=${DOMAIN}
      - DNS_PROVIDER=${DNS_PROVIDER}
      - CF_API_EMAIL=${CLOUDFLARE_EMAIL}
      - CF_API_KEY=${CLOUDFLARE_API_KEY}
      - TZ=${TZ}
    domainname: ${DOMAIN}
    command:
      # Globals    
      - --api=true      # ^ Enabling Traefik API - In production API, Access should be secured 
                        # by authentication and authorizations if exposed outside the network
                        # (https://doc.traefik.io/traefik/operations/api/#configuration)
      - --global.checkNewVersion=true               # ? Periodically check for update - (Default: true)
      - --log.level=DEBUG                           # ? Log level - (Default: ERROR) other logging levels are DEBUG, PANIC, FATAL, ERROR, WARN, and INFO.
      - --log.filePath=/logs/traefik.log            # ? Log path - optional - related to volume /logs defined above
      - --accessLog.filePath=/logs/access.log       # ? Log path - optional - related to volume /logs defined above
      - --accessLog.bufferingSize=100               # ? Log size - optional

      # Docker
      - --providers.docker=true                     # ^ Enable Docker provider - other providers (https://doc.traefik.io/traefik/providers/overview)
      - --providers.docker.exposedbydefault=false   # ^ Expose only containers that have labels setup (https://doc.traefik.io/traefik/providers/docker/#exposedbydefault)
      - --providers.docker.endpoint=unix:///var/run/docker.sock # ^ Traefik requires access to the Docker socket in order to get its dynamic configuration from there - related to volume defined above

      # Entrypoints
      - --entryPoints.console.address=:8080        # ^ Defining port 8080 as "alias" called "console" - this port will be reachable from outside of Traefik container, uncomment for debugging purpose
      - --entryPoints.web.address=:80               # ^ Defining port 80 as "alias" called "web" - this port will be reachable from outside of Traefik container
      - --entrypoints.websecure.address=:443        # ^ Defining port 443 as "alias" called "websecure" - this port will be reachable from outside of Traefik container

      # Redirection to SSL
      - --entryPoints.web.http.redirections.entryPoint.to=websecure     # ^ If trying to access service using port 80 redirect to 443
      - --entryPoints.web.http.redirections.entryPoint.scheme=https     # ^ If trying to access service using http redirect to https
      - --entryPoints.web.http.redirections.entrypoint.permanent=true   # ^ Apply a permanent redirection.

      # LetsEncrypt (https://doc.traefik.io/traefik/user-guides/docker-compose/acme-tls/)
      - --certificatesResolvers.letsencrypt.acme.storage=acme/acme.json   # ^ Storage location where ACME certificates are going to be saved, this work with conjunction to volume definer above.
     #- --certificatesresolvers.letsencrypt.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory    # ! Let's Encrypt Staging Server, comment out after testing - (https://doc.traefik.io/traefik/https/acme/#caserver) - highly recommend that you config works ok in staging before using Let's Encrypt live servers. In case of failures in this config you might be banned by Let's Encrypt for a while for abusing their live servers with faulty configuration requests.     
      - --certificatesResolvers.letsencrypt.acme.dnsChallenge=true    # * DNS challenge, there are other ways of proving that you owned domain name defined below (https://doc.traefik.io/traefik/https/acme/#dnschallenge)
      - --certificatesResolvers.letsencrypt.acme.dnsChallenge.provider=cloudflare   # * Find your provider (https://doc.traefik.io/traefik/https/acme/#providers) and replace cloudflare with the one you use. This corresponds to "environment:" variables defined earlier. 
     #- --providers.file=true   # ? Enable file provider if you need it.
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro    # If you use Docker Socket Proxy, comment this line out
      - ${TRAEFIK_DATA_VOLUME}/acme:/acme   # cert location - you must create this empty file and change permissions to 600
      - ${TRAEFIK_DATA_VOLUME}/logs:/logs   # for fail2ban or crowdsec
    labels:
      - traefik.enable=true   # ^ Enabling Traefik container to be exposed by itself
      - traefik.http.middlewares.admin.basicauth.users=${TRAEFIK_DASH_USER}:${TRAEFIK_DASH_PWD}   # * Generate SHA1 to protect access to the Web UI here: https://hostingcanada.org/htpasswd-generator - on this page I used: user/password = admin/greenfrog and got htpasswd: admin:{SHA}/jIOs1SoLMVGd6FMOlt5mF6Ega0=  (https://doc.traefik.io/traefik/middlewares/basicauth/). You can reuse this line multiple times under different containers to protect access to them.  

      # Define route/router called "thisproxylocal"
      - traefik.http.routers.thisproxylocal.rule=Host(`${TRAEFIK_SERVER_IP}`)   # * Change xxx.xxx.xxx.xxx to your Docker server IP
      - traefik.http.routers.thisproxylocal.entryPoints=console                 # ^ Traefik WebUI is by default exposed on port 8080 so we have to redirect all requests to that port by creating entryPoint equal to "console" - alias that we defined several lines above.
      - traefik.http.routers.thisproxylocal.service=api@internal                # ^ Enable WebUI service on this specific router.
      - traefik.http.routers.thisproxylocal.middlewares=admin                   # ^ Enabling authentication on this specific router.

      # Define route/router called "thisproxytls"
      - traefik.http.services.thisproxytls.loadbalancer.server.port=8080    # ^ Define loadBalancer port for WebUI
      - traefik.http.routers.thisproxytls.rule=Host(`${TRAEFIK_DASH_DOMAIN}`)    # * Define URL that will be redirected to this container on port 8080 from https
      - traefik.http.routers.thisproxytls.entrypoints=websecure             # ^ Just because we defined redirection where any request from the Internet received on port 80 - http will be redirected to port 443 https we open websecure entrypoint as this is from where we will be receiving all the traffick anyway.
      - traefik.http.routers.thisproxytls.service=api@internal              # ^ Enable WebUI service on this specific router.
      - traefik.http.routers.thisproxytls.middlewares=admin                 # ^ Enabling authentication on this specific router.
      - traefik.http.routers.thisproxytls.tls.certresolver=letsencrypt      # ^ Use Let's Encrypt resolver for TLS certification
    ports:
      - 80:80
      - 443:443
    networks:
      - web

networks:
  web:
    name: web
