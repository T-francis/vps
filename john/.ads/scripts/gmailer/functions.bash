#!/bin/bash

#<run-gmailer><name>( internal )
#<run-gmailer><description> call the mailer container
#<run-gmailer><args> "$subject" "$message" "$recipient"
function run-gmailer() {

    local subject=${1}
    local message=${2}
    local recipient=${3}

    docker run \
        --rm \
        -e EMAIL=${GMAILER_EMAIL} \
        -e AUTHPASS=${GMAILER_AUTHPASS} \
        -e MESSAGE="${message}" \
        -e SUBJECT="${subject}" \
        -e TO="${recipient}" \
        aliart/docker-mailer
}

#<gmailer><name>( exposed )
#<gmailer><description> a simple mailer that work with a gmail account 
#<gmailer><args> --subject|-s "string" --message|-m "string" --recipient|-r "string" || SUBSCRIBERS
function gmailer() {

    OPTS=$( getopt -o s:,m:,r: -l subject:,message:,recipient: -- "$@" )
    [[ $? != 0 ]] && echo "Failed parsing options." >&2  && exit 1
    eval set -- "$OPTS"
    while true ;
    do
        case ${1} in
            --subject | -s)
                local subject=${2}
                shift 2;
                ;;
            --message | -m)
                local message=${2}
                shift 2;
                ;;
            --recipient | -r)
                local recipient=${2}
                shift 2;
                ;;
            --)
                shift;
                break
                ;;
        esac
    done

    # required args
    [[ -z ${message} ]] || [[ -z ${recipient} ]] \
        && echo "Error gmailer: missing message or recipient" \
        && exit 1

    [[ -z ${subject} ]] && local subject=${GMAILER_DEFAULT_SUBJECT}

    if [ "${recipient}" = "SUBSCRIBERS" ]; then

        [[ ! -f "${GMAILER_SUBSCRIBERS}" ]] && echo "Error: GMAILER_SUBSCRIBERS not found" && exit 0

        [[ $(eof-is-newline ${GMAILER_SUBSCRIBERS}) = false ]] && echo >> ${GMAILER_SUBSCRIBERS} #empty line to be fully read

        while read addresse; do
            case "$addresse" in \#*) continue ;; esac #comment skipper
            [[ ! "${addresse}" = "" ]] && run-gmailer "$subject" "$message" "$addresse"
        done < ${GMAILER_SUBSCRIBERS}

    else
        run-gmailer "$subject" "$message" "$recipient"
    fi

}
