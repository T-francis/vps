-- USER
CREATE USER IF NOT EXISTS `${username}`@'localhost' IDENTIFIED BY '${password}';
CREATE USER IF NOT EXISTS `${username}`@'%' IDENTIFIED BY '${password}';

-- DATABASES & PRIVILEGES
CREATE DATABASE IF NOT EXISTS ${dbname};
USE ${dbname}; SET character_set_server = 'utf8mb4'; SET collation_server = 'utf8mb4_unicode_ci';
GRANT ALL ON ${dbname}.* TO `${username}`@'localhost';
GRANT ALL ON ${dbname}.* TO `${username}`@'%';