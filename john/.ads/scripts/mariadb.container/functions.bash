#!/bin/bash

#<mariadb-create-db><name>( exposed )
#<mariadb-create-db><description> create database/user to the mariadb container
#<mariadb-create-db><args> $dbname $username $password
function mariadb-create-db() {
    mariadb ${1} ${2} ${3} create
    exit 0
}

#<mariadb-drop-db><name>( exposed )
#<mariadb-drop-db><description> drop database/user to the mariadb container
#<mariadb-drop-db><args> $dbname $username $password
function mariadb-drop-db() {
    mariadb ${1} ${2} ${3} drop
    exit 0
}

#<mariadb<name>( internal )
#<mariadb<description> build args and call script for -create-db -drop-db
#<mariadb<args> $dbname $username $password
function mariadb() {

    local help="mariadb usage\n \
            This function may be call to create/remove database/user to the mariadb container\n \
            mariadb-create-db [options] || mariadb-drop-db [options] \
            --help or -h: print current help function\n \
            --dbname=(set the db name)\n \
            --username=(the user name)\n \
            --password=(the user password)"
        
    local action=${4}
    # if no args, displaying help
    if [ $# -eq 0 ];then echo -e ${help} && exit 0; fi
        
    OPTS=$( getopt -o h,db:,u:,p: -l help,dbname:,username:,password: -- "$@" )
    if [ $? != 0 ];then	exit 1; fi
    eval set -- "$OPTS"

    while true ;
    do
        case "$1" in
            --help | -h)
                echo -e ${help}
                exit 0
                ;;
            --dbname | -db)
                local dbname=${2}
                shift 2
                ;;
            --username | -u)
                local username=${2}
                shift 2
                ;;
            --password | -p)
                local password=${2}
                shift 2
                ;;
            --)
                shift;
                break
                ;;
        esac
    done

    [[ ${dbname} = '' ]] \
        || [[ ${username} = '' ]] \
        || [[ ${password} = '' ]] \
        && echo -e "Script error, missing param" \
        && exit 1;

    # the 4th arg is sent inernally by parent function
    local sql=$(cat ${SCRIPTS_DIR}/mariadb.container/${action}.db.sql \
        | sed s/\${dbname}/${dbname}/g \
        | sed s/\${username}/${username}/g \
        | sed s/\${password}/${password}/g )

    docker exec -i mariadb mysql -uroot -p${MYSQL_ROOT_PASSWORD} <<< ${sql} \
        && exit 0

}

#<mariadb-dump><name>( exposed )
#<mariadb-dump><description> dump database(s) into STORAGES_DIR/dumps/mysql
#<mariadb-dump><args> $dbname
function mariadb-dump(){

    # without arg dump every db
    [[ $# -eq 0  ]] \
        && mariadb-dump-multiple \
        && exit 0

    # else dump only wanted one
    for dbname in "$@"
    do
        mariadb-dump-single ${dbname}
    done
    exit 0
}

#<mariadb-dump-single><name>( internal )
#<mariadb-dump-single><description> dump a single database into STORAGES_DIR/dumps/mysql
#<mariadb-dump-single><args>
function mariadb-dump-single() {

    local dbname=${1}
    select='SELECT `SCHEMA_NAME` from `INFORMATION_SCHEMA`.`SCHEMATA` WHERE `SCHEMA_NAME` ="'${dbname}'";';
    result=$(docker exec -i mariadb mysql -uroot -p${MYSQL_ROOT_PASSWORD} <<< ${select})

    # exit if database not found
    # quote style to avoid too many args warn
    if [ -z "$result" ]
    then
        echo "Access error OR no databases found for ${dbname}"
    else
        docker exec -i mariadb mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} ${dbname} > ${STORAGES_DIR}/dumps/mysql/${dbname}.sql \
                && echo "Simple dump for ${dbname} done"
    fi
    exit 0
}

#<mariadb-dump-multiple><name>( internal )
#<mariadb-dump-multiple><description> dump multiple databases into STORAGES_DIR/dumps
#<mariadb-dump-multiple><args>
function mariadb-dump-multiple() {

    bases=`docker exec -i mariadb mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "show databases;" -B -s 2> /dev/null`

    if [ -z "$bases" ];then echo "Access error OR no databases found"; exit 1; fi # quote style to avoid too many args warn

    echo "Dump start"
    for base in ${bases}
    do
        [[ ! ${base} = information_schema ]] \
            && [[ ! ${base} = mysql ]] \
            && [[ ! ${base} = performance_schema ]] \
            && echo "Backing up $base..." \
            && docker exec -i mariadb mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} ${base} > ${STORAGES_DIR}/dumps/mysql/${base}.sql \
            || echo "Skipping $base..."
    done
    echo "Dump done"
    exit 0
}