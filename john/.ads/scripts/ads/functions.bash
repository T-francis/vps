#!/bin/bash

#<find-function><name>( internal )
#<find-function><description> get the location of a script by function name
#<find-function><args> $targetFunction
function find-function(){
    local targetFunction=${1}
    find "${SCRIPTS_DIR}" -name '*.*sh' -print0 2>/dev/null|
    while IFS= read -r -d $'\0' file; do
        grep -E "(#<$targetFunction><name>\\( exposed \\).?*)" ${file} | while read -r name ; do      
            local escapedName="$name" \
                    && escapedName="${escapedName#"#<"}" \
                    && escapedName="${escapedName%"><name>( exposed )"}";
            [[ ${targetFunction} = ${escapedName} ]] && echo ${file};
        done
    done
    exit 0
}

#<list-all-exposed-function><name>( exposed )
#<list-all-exposed-function><description> list all exposed function
#<list-all-exposed-function><args>
function list-all-exposed-function(){
    find "${SCRIPTS_DIR}" -name '*.*sh' -print0 2>/dev/null|
    while IFS= read -r -d $'\0' file; do
        local namespace=$( basename $( dirname ${file}) );
        grep -E "(#<.?*><name>\\( exposed \\).?*)" ${file} | while read -r name ; do  
            local escaped_name="$name" \
                    && escaped_name="${escaped_name#"#<"}" \
                    && escaped_name="${escaped_name%"><name>( exposed )"}";
                echo "${escaped_name}";
        done
    done
    exit 0;
}

#<describe><name>( exposed )
#<describe><description> describe all exposed function
#<describe><args> $namespace?
function describe(){

    local namespace=${1}
    local location=""

    [[ -d "${SCRIPTS_DIR}/${namespace}" ]] \
        && location=${SCRIPTS_DIR}/${namespace} \
        || location=${SCRIPTS_DIR}

    [[ ! -z "${namespace}" ]] \
        && [[ ! -d "${SCRIPTS_DIR}/${namespace}" ]] \
        && echo "Error: namespace not found" \
        && exit 1

    echo -e "info: ? and ! mean ?optional or !mandatory\n";

    find "${location}" -name '*.*sh' -print0 2>/dev/null|

    while IFS= read -r -d $'\0' file; do
        local namespace=$( basename $( dirname ${file}) );
        echo -e "${namespace%.*}/$( basename ${file}):\n";

        grep -E "(#<.?*><name>\\( exposed \\).?*)" ${file} | while read -r name ; do
            escaped_name="$name" \
                && escaped_name="${escaped_name#"#<"}" \
                && escaped_name="${escaped_name%"><name>( exposed )"}";

            grep -E "(#<${escaped_name}><description>.?*)" ${file} | while read -r description ; do
                escaped_description="$description" \
                    && escaped_description="${escaped_description#"#<${escaped_name}><description>"}";

                grep -E "(#<${escaped_name}><args>.?*)" ${file} | while read -r args ; do
                    escaped_args="$args" \
                        && escaped_args="${escaped_args#"#<${escaped_name}><args>"}";
                    echo -e "      - ${escaped_name}\n        Description:${escaped_description}\n        Usage: ${escaped_name}${escaped_args}\n";

                done

            done

        done

    done
    exit 0;
}

#<is-installed><name>( exposed )
#<is-installed><description> check if a package is installed (work with dpkg)
#<is-installed><args> $packageName
function is-installed() {
    # MUST check arg is not empty
    dpkg -s ${1} &> /dev/null; [[ $? -eq 0 ]] && echo true || echo false;
    exit 0
}

#<eof-is-newline><name>( exposed )
#<eof-is-newline><description> check if there is a empty line at end of file
#<eof-is-newline><args> $fileLocation
function eof-is-newline() {
    # MUST check arg is not empty and is a file
    [[ -z "$(tail -c 1 "${1}")" ]] && echo true || echo false;
    exit 0
}