#!/bin/bash

#<gupload-backup><name>( exposed )
#<gupload-backup><description> backup files registered in a config file to a google drive
#<gupload-backup><args>
function gupload-backup(){

    [[ ! -f "${BACKUP_LIST}" ]] \
        && echo "Error: backup.list not found" && exit 0 \
        || echo "Info: reading ${BACKUP_LIST}";

    [[ $(eof-is-newline ${BACKUP_LIST}) = false ]] && echo >> ${BACKUP_LIST} #empty line to be fully read

    # ignoring commented line parsing (#)
    while read originalFile; do

        case "$originalFile" in \#*) continue ;; esac #comment skipper

        if [ -d "${originalFile}" ]; then
            pathTo="/$(basename ${originalFile})"
        elif [ -f "${originalFile}" ]; then
            pathTo="/$(basename $(dirname ${originalFile}))/$(basename ${originalFile})"
        else
            echo "Warning: ${originalFile} is an invalid location or doesn't exist, nothing will be done"
        fi

        # call gupload to send on google drive if pathTo get defined
        [[ ! -z ${pathTo} ]] \
            && gupload ${originalFile} -c ${pathTo} && exit 0 \
            || echo "Info: nothing to upload" && exit 0

    done < ${BACKUP_LIST}

}