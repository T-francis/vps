#!/bin/bash

#<docker-connect><name>( exposed )
#<docker-connect><description> open a shell into a container
#<docker-connect><args> $dockerExecArgs
function docker-connect() {
    docker exec -it "$@" sh -c "stty rows 50 && stty cols 150 && bash";
}

#<docker-logs><name>( exposed )
#<docker-logs><description> docker logs alias
#<docker-logs><args> $dockerLogsArgs
function docker-logs() {
    docker logs "$@";
}